// [SECTION] Dependencies and Modules
	
	const Course = require('../models/Course');

// [SECTION] Functionality[Create]
	module.exports.createCourse = (info) => {
		
		let courseName = info.name;
		let courseDesc = info.description;
		let coursePrice = info.price;

		let newCourse = new Course({
			name: courseName,
			description: courseDesc,
			price: coursePrice
		})
		return newCourse.save().then((savedCourse, error) => {
			if (error){
				return 'Failed to Save New Document';
			} else {
				return savedCourse;
			}
		});

	};

// [SECTION] Functionality[Retrieve]
	
	module.exports.getAllCourse = () => {
		

		return Course.find({}).then(result => {

			return result;

		});
	};

	
	module.exports.getCourse = (courseid) => {
		
		return Course.findById(courseid).then(resultofQuery => {

			return resultofQuery;

		});
	};

	
	module.exports.getAllActiveCourse = () => {
		
		return Course.find({isActive:true}).then(resultoftheQuery => {
			return resultoftheQuery;
		});
	}; 

// [SECTION] Functionality[Update]
	
	module.exports.updateCourse = (courseid, details) => {
		
      let cName = details.name; 
      let cDesc = details.description;
      let cCost = details.price;   
       
      let updatedCourse = {
        name: cName,
        description: cDesc,
        price: cCost
      }
      
      return Course.findByIdAndUpdate(courseid, updatedCourse).then((courseUpdated, err) => {
          
          if (err) {
            return 'Failed to update Course'; 
          } else {
            return 'Successfully Updated Course'; 
          }
      });

	}

	// [Deactivate]

   module.exports.deactivateCourse = (id) => {
     let updates = {
       isActive: false
     } 
     
     return Course.findByIdAndUpdate(id, updates).then((archived, error) => { 
        if (archived) {
          return `The Course ${id} has been deactivated`;
        } else {
           return 'Failed to archive course'; 

        };
     });
   };

  // [Reactivate]

  module.exports.reactivateCourse = (id) => {
     let updates = {
       isActive: true
     } 
     
     return Course.findByIdAndUpdate(id, updates).then((archived, error) => { 
        if (archived) {
          return `The Course ${id} has been reactivated`;
        } else {
           return 'Failed to reactivate course'; 

        };
     });
   };

// [SECTION] Functionality[Delete]
	
  module.exports.deleteCourse = (courseid) => {
    
    return Course.findByIdAndRemove(courseid).then((removedCourse, err) => {
        
        if (err) {
          return 'No Course Was Removed'; 
        } else { 
          return 'Course Succesfully Deleted'; 
        };
    });
  };

// [SECTION] Dependencies and Modules
	
	const exp = require("express");
	const controller = require('../controllers/courses');

// [SECTION] Routing Component
	
	const route = exp.Router();
	

// [SECTION] [POST] Routes

	route.post('/create', (req, res) => {
		
		let data = req.body;
		
		controller.createCourse(data).then(outcome => {
			res.send(outcome);
		});

	});
	

// [SECTION] [GET] Routes
	
	route.get('/all',(req, res) => {
		
		controller.getAllCourse().then(outcome => {
			
			res.send(outcome);

		});
	});

	
	route.get('/:courseid', (req, res) => {
	
		let courseID = req.params.courseid;
		
		controller.getCourse(courseID).then(result => {
			
			res.send(result);
		});
	});

	
	route.get('/',(req, res) => {
		
		controller.getAllActiveCourse().then(outcome => {
			
			res.send(outcome);
		});
	});

// [SECTION] [PUT] Routes
	// Assign a route for the update course function
	route.put('/:courseid', (req, res) => {
		
      let courseId = req.params.courseid; 
      let details = req.body;
      
      let cName = details.name; 
      let cDesc = details.description;
      let cCost = details.price;       
      if (cName  !== '' && cDesc !== '' && cCost !== '') {
        controller.updateCourse(courseId, details).then(outcome => {
            res.send(outcome); 
        });      
      } else {
        res.send('Incorrect Input, Make sure details are complete');
      }

	});

	// [Deactivate]
  route.put('/:courseid/archive', (req, res) => {
        
      let courseId = req.params.courseid; 

      controller.deactivateCourse(courseId).then(resultOfTheFunction => {
          
          res.send(resultOfTheFunction);
      });
  });

  // [Reactivate]
  route.put('/:courseid/reactivate', (req, res) => {
        
      let courseId = req.params.courseid; 

      controller.reactivateCourse(courseId).then(resultOfTheFunction => {
          
          res.send(resultOfTheFunction);
      });
  });


// [SECTION] [DEL] Routes
	
	route.delete('/:courseid', (req,res) => {
		
		let courseId = req.params.courseid;

		controller.deleteCourse(courseId).then(outcome => {
			res.send(outcome);

		});
	});

// [SECTION] Export Route System
	module.exports = route;
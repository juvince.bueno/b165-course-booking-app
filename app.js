// [SECTION] Packages and Dependencies
	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const courseRoutes = require('./routes/courses');
	const userRoutes = require('./routes/users');


// [SECTION] Server Setup
	const app = express();
	dotenv.config();
	app.use(express.json());
	const connString = process.env.CONNECTION_STRING;
	const port = process.env.PORT;

// [SECTION]Application Routes
	
	app.use('/courses', courseRoutes);
	app.use('/users', userRoutes);


// [SECTION] Database Connect
	mongoose.connect(connString);

	let connectionStatus = mongoose.connection;

	connectionStatus.on('open', () => console.log('Database is Connected'));

// [SECTION] Gateway Response
	app.get('/', (req, res) => {
		res.send(`Welcome to Vince's Cabayugan Learners Information API`);
	});
	app.listen(port, () => console.log(`Server is running on port ${port}`));